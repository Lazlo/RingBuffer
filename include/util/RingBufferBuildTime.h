#ifndef D_RingBufferBuildTime_H
#define D_RingBufferBuildTime_H

///////////////////////////////////////////////////////////////////////////////
//
//  RingBufferBuildTime is responsible for recording and reporting when
//  this project library was built
//
///////////////////////////////////////////////////////////////////////////////

class RingBufferBuildTime
  {
  public:
    explicit RingBufferBuildTime();
    virtual ~RingBufferBuildTime();
    
    const char* GetDateTime();

  private:
      
    const char* dateTime;

    RingBufferBuildTime(const RingBufferBuildTime&);
    RingBufferBuildTime& operator=(const RingBufferBuildTime&);

  };

#endif  // D_RingBufferBuildTime_H
