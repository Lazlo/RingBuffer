#ifndef D_RingBuffer_H
#define D_RingBuffer_H

/**********************************************************************
 *
 * RingBuffer is responsible for ...
 *
 **********************************************************************/

typedef struct RingBufferStruct * RingBuffer;

RingBuffer RingBuffer_Create(unsigned size);
void RingBuffer_Destroy(RingBuffer);

unsigned RingBuffer_GetSize(RingBuffer);
unsigned RingBuffer_GetFree(RingBuffer);
void RingBuffer_Enqueue(RingBuffer);
void RingBuffer_Dequeue(RingBuffer);

#endif  /* D_FakeRingBuffer_H */
