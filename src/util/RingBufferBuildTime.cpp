#include "RingBufferBuildTime.h"

RingBufferBuildTime::RingBufferBuildTime()
: dateTime(__DATE__ " " __TIME__)
{
}

RingBufferBuildTime::~RingBufferBuildTime()
{
}

const char* RingBufferBuildTime::GetDateTime()
{
    return dateTime;
}

