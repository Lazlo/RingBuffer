#include "RingBuffer.h"
#include <stdlib.h>
#include <memory.h>

typedef struct RingBufferStruct
{
	unsigned size;
	unsigned head;
	unsigned tail;
} RingBufferStruct;

RingBuffer RingBuffer_Create(unsigned size)
{
	RingBuffer self = calloc(1, sizeof(RingBufferStruct));
	self->size = size;
	self->head = 0;
	self->tail = 0;
	return self;
}

void RingBuffer_Destroy(RingBuffer self)
{
	free(self);
}

unsigned RingBuffer_GetSize(RingBuffer self)
{
	return self->size;
}

unsigned RingBuffer_GetFree(RingBuffer self)
{
	return self->size - (self->tail - self->head);
}

void RingBuffer_Enqueue(RingBuffer self)
{
	self->tail += 1;
}

void RingBuffer_Dequeue(RingBuffer self)
{
	self->head += 1;
}
