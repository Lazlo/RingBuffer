extern "C"
{
#include "RingBuffer.h"
}

#include "CppUTest/TestHarness.h"

#define RING_BUFFER_SIZE 8

TEST_GROUP(RingBuffer)
{
	RingBuffer ringBuffer;

	void setup()
	{
		ringBuffer = RingBuffer_Create(RING_BUFFER_SIZE);
	}

	void teardown()
	{
		RingBuffer_Destroy(ringBuffer);
	}
};

TEST(RingBuffer, GetSize_equalsSizePassedToCreate)
{
	CHECK_EQUAL(RING_BUFFER_SIZE, RingBuffer_GetSize(ringBuffer));
}

TEST(RingBuffer, GetFree_equalsSizePassedToCreate_afterCreate)
{
	CHECK_EQUAL(RING_BUFFER_SIZE, RingBuffer_GetFree(ringBuffer));
}

TEST(RingBuffer, Enqueue_decrementsFree_whenBufferIsNotFull)
{
	RingBuffer_Enqueue(ringBuffer);
	CHECK_EQUAL(RING_BUFFER_SIZE - 1, RingBuffer_GetFree(ringBuffer));
}

TEST(RingBuffer, Dequeue_incrementsFree_whenBufferWasNotEmpty)
{
	RingBuffer_Enqueue(ringBuffer);
	RingBuffer_Dequeue(ringBuffer);
	CHECK_EQUAL(RING_BUFFER_SIZE, RingBuffer_GetFree(ringBuffer));
}
