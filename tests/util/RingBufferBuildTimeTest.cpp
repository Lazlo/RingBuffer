#include "CppUTest/TestHarness.h"
#include "RingBufferBuildTime.h"

TEST_GROUP(RingBufferBuildTime)
{
  RingBufferBuildTime* projectBuildTime;

  void setup()
  {
    projectBuildTime = new RingBufferBuildTime();
  }
  void teardown()
  {
    delete projectBuildTime;
  }
};

TEST(RingBufferBuildTime, Create)
{
  CHECK(0 != projectBuildTime->GetDateTime());
}

